package stud11418018.develops.pesanantar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

public class DashboardPengelola extends AppCompatActivity {
    Button btn_tambah_menu2,btn_tambah_menu;
    ImageView imageView8;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_pengelola);

        btn_tambah_menu2= (Button) findViewById(R.id.btn_tambah_menu2);
        btn_tambah_menu= (Button) findViewById(R.id.btn_tambah_menu);
        imageView8= (ImageView) findViewById(R.id.imageView8);
    }
    public void tambahmenu (View view) {
        Intent intent = new Intent(DashboardPengelola.this, tambahmakanan.class);
        startActivity(intent);
    }

    public void tambahmenu1 (View view) {
        Intent intent = new Intent(DashboardPengelola.this, tambahmakanan.class);
        startActivity(intent);
    }
    public void edit (View view) {
        Intent intent = new Intent(DashboardPengelola.this, EditDataRestaurant.class);
        startActivity(intent);
    }
}
