package stud11418018.develops.pesanantar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import stud11418018.develops.pesanantar.data1.PengelolaDao;
import stud11418018.develops.pesanantar.data1.PengelolaDataBase;
import stud11418018.develops.pesanantar.model1.Pengelola;

public class loginPengelola extends AppCompatActivity {

    EditText editTextEmail, editTextPassword;
    Button buttonLogin;
    TextView textViewRegister;
    PengelolaDao db;
    PengelolaDataBase dataBase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        editTextEmail = findViewById(R.id.editTextEmail);
        editTextPassword = findViewById(R.id.editTextPassword);
        buttonLogin = findViewById(R.id.buttonLogin);

        textViewRegister = findViewById(R.id.textViewRegister);

        dataBase = Room.databaseBuilder(this, PengelolaDataBase.class, "mi-database.db")
                .allowMainThreadQueries()
                .build();

        db = dataBase.getPengelolaDao();


        textViewRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(loginPengelola.this, Register_Pengelola.class));
            }
        });

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = editTextEmail.getText().toString().trim();
                String password = editTextPassword.getText().toString().trim();

                Pengelola pengelola = db.getPengelola(email, password);
                if (pengelola != null) {
                    Intent i = new Intent(loginPengelola.this, DashboardPengelola.class);
                    i.putExtra("Pengelola", pengelola);
                    startActivity(i);
                    finish();
                }else{
                    Toast.makeText(loginPengelola.this, "Unregistered user, or incorrect", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}



